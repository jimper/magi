import itertools
import os
from datetime import datetime

import pyftdc
from bokeh.palettes import Viridis256


def get_metrics_in_range(file_name, metrics_names, start_timestamp=None, end_timestamp=None):
    """
    Get metrics from a dir or file.
    :param file_name: Path to file or directory with FTDC metrics files.
    :param metrics_names: A list of metrics names, units and colors.
    :param start_timestamp: start timestamp for range.
    :param end_timestamp: end timestamp for range.
    :return: a tuple with timestamp and metrics dictionary.
    """
    datasets = get_datasets_all(metrics_path=file_name)

    # Construct metrics dictionary
    metrics_dict = dict()
    for metric in metrics_names:
        name, color, units = metric
        metrics_dict[name] = {
            'metrics': [],
            'units': units,
            'color': color,
        }

    # get all timestamps just because
    all_timestamps = []
    for ds in datasets:
        t = ds.get_metrics('start')
        all_timestamps.extend(t)
        t = None

        for name in metrics_dict:
            metric = ds.get_metrics(name)
            if metric:
                metrics_dict[name]['metrics'].extend(metric)
                metric = None

    # Make real timestamps
    KB = 1024
    MB = KB * KB
    all_timestamps = [int(t/1000) for t in all_timestamps]
    for name in metrics_dict:
        if metrics_dict[name]['units'] == 'KB':
            metrics_dict[name]['metrics'] = [t/KB for t in metrics_dict[name]['metrics']]
        if metrics_dict[name]['units'] == 'MB':
            metrics_dict[name]['metrics'] = [t/MB for t in metrics_dict[name]['metrics']]

    return all_timestamps, metrics_dict


def get_datasets_all(metrics_path):
    """
    Return a list of datasets from the path. This can be a directory.
    :param metrics_path: A directory containing FTDC metrics files, or a single FTDC metrics file.
    :return: A list of Datasets objects.
    """
    datasets = []
    ftdc_parser = pyftdc.FTDCParser()
    if os.path.isfile(metrics_path):
        now = datetime.now()
        current_time = now.strftime("%H:%M:%S")
        print(f"{current_time} Start parsing {metrics_path}")
        ds = ftdc_parser.read_file(metrics_path)
        datasets.append(ds)
        now = datetime.now()
        current_time = now.strftime("%H:%M:%S")
        print(f"{current_time} End parsing")

    if os.path.isdir(metrics_path):  # oh boy
        now = datetime.now()
        current_time = now.strftime("%H:%M:%S")
        print(f"{current_time} Start parsing {metrics_path}")
        datasets = ftdc_parser.read_dir(metrics_path)
        now = datetime.now()
        current_time = now.strftime("%H:%M:%S")
        print(f"{current_time} End parsing")

    return datasets


def output_metrics_names(metrics_file, metrics_csv):
    """
    Writes the metrics names found in an FTDC metrics file to a CSV file, with a color.
    :param metrics_file: path to the metrics file.
    :param metrics_csv: path to the CSV file to write to.
    :return: The number of entries written to the CSV file.
    """
    # A sequence of 256 colors
    viridis_colors = Viridis256
    colors = itertools.cycle(viridis_colors)

    ftdc_parser = pyftdc.FTDCParser()
    ds = ftdc_parser.read_file(metrics_file)
    if ds:
        with open(metrics_csv, 'w') as csv:
            for metric in ds.metrics_names:
                c = next(colors)
                csv.write('{},{}\n'.format(metric, c))
        return len(ds.metrics_names)

    return None

