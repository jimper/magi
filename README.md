# magi

Data visualization of metrics from MongoDB FTDC files. It can output a self-contained HTML file with selected metrics.
An example use for the pip pyftdc package.

## Usage

```bash
python3 magi.py [-h] --input-file INPUT_FILE --metrics-config METRICS_CONFIG
```

where
- --input-file
 Path to single mongodb diagnostics file. Required.
- --metrics-config 
 Path to a CSV file with metrics names and colors. Defaults to `metrics.csv`
- -h --help
 Prints usage and exits.

## Usage

```bash
python3 magi.py [options]
```

where the options are
  -h, --help            show this help message and exit
  --input-file INPUT_FILE
                        Path to single mongodb diagnostics file.
  --input-dir INPUT_DIR
                        Path to a directory with mongodb diagnostic files.
  --metrics-config METRICS_CONFIG
                        Path to a CSV file with metrics names and colors.
  --output-html OUTPUT_HTML
                        Path to save output as HTML file.
  --output-metrics OUTPUT_METRICS
                        Output metrics CSV and suggested colors


## Getting started

- Create virtual environment and activate
```bash
python -m venv venv --prompt magi
source ./venv/bin/activate
```
- Install required packages:
```bash
pip install -r requirements.txt
```
- Play.
```bash
python3 magi.py --input-file  <ftdc-file>
```
- A browser window will appear with the graphs in the hardcoded metrics:
![Screenshot](./Screenshot.png?raw=true)


### The format of metrics.csv
The metrics csv file contains a list of the names of the metrics, with the color it should be plotted.
The metrics names can be prepended with a '@' to be converted in rates (i.e.: bytes per second instead of bytes).

These lines will plot two metrics in red and blue:
```bash
serverStatus.network.bytesIn,red
serverStatus.network.bytesOut,blue
```

These lines will output the metrics rated by second, same colors:
```bash
@serverStatus.network.bytesIn,red
@serverStatus.network.bytesOut,blue
```

## License
Apache V2.

## Project status
Active
