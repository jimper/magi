import pandas as pd
from bokeh.io import show, output_file
from bokeh.models import ColumnDataSource
from bokeh.plotting import figure

from ftdc_metrics import output_metrics_names, get_metrics_in_range


def plot_graph(width, height, group, html_file):

    timestamps, metrics_dict = group

    # Timestamps to column data source
    date_times = pd.to_datetime(timestamps, unit='s')
    cds = ColumnDataSource(data={
        'date': date_times,
    })

    for metric in metrics_dict:
        values = metrics_dict[metric]['metrics']
        cds.add(values, metric)

    # For Jupyter Notebooks
    # output_notebook()
    if html_file:
        output_file(html_file)

    plot = figure(x_axis_type='datetime', width=width, height=height,
                  title='FTDC Metrics',
                  tools="pan,wheel_zoom,box_zoom,reset",
                  toolbar_location='above')  # above, below, left, right

    for name in metrics_dict:
        color = metrics_dict[name]['color']
        plot.line(x='date', legend_label=name, y=name, source=cds, color=color)

    show(plot)


def read_metrics_csv(metrics_file_name):
    # return pd.read_csv(metrics_file_name)
    metrics = []
    with open(metrics_file_name) as csv:
        for line in csv:
            t = line.strip().split(',')
            metrics.append(tuple(t))
    return metrics


if __name__ == '__main__':
    import argparse

    parser = argparse.ArgumentParser(
        description='Display FTDC metrics for a given FTDC metrics file.')
    parser.add_argument('--input-file', required=False,
                        help='Path to single mongodb diagnostics file.', type=str)
    parser.add_argument('--input-dir', required=False,
                        help='Path to a directory with mongodb diagnostic files.', type=str)
    parser.add_argument('--metrics-config', required=False, default='metrics.csv',
                        help='Path to a CSV file with metrics names and colors.', type=str)
    parser.add_argument('--output-html', required=False,
                        help='Path to save output as HTML file.', type=str)
    parser.add_argument('--output-metrics', required=False,
                        help='Output metrics CSV and suggested colors', type=str)
    parser.add_argument('--width', required=False, default=800,
                        help='Frame width', type=int)
    parser.add_argument('--height', required=False, default=400,
                        help='Frame height', type=int)

    args = parser.parse_args()

    if not args.input_file and not args.input_dir:
        print('Please provide either --input-file or --input-dir.')
    else:
        metrics_config = read_metrics_csv(args.metrics_config)

        if args.input_file:
            file_name = args.input_file
        else:
            file_name = args.input_dir

        if args.output_metrics:
            output_metrics_names(file_name, args.output_metrics)
        else:
            g = get_metrics_in_range(file_name, metrics_config)

            plot_graph(args.width, args.height, g, args.output_html)
