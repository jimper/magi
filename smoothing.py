import numpy as np


def moving_average_smoothing(X, k):
    S = np.zeros(X.shape[0])
    for t in range(X.shape[0]):
        if t < k:
            S[t] = np.mean(X[:t + 1])
        else:
            S[t] = np.sum(X[t - k:t]) / k
    return S


def exponential_smoothing(X, alpha):
    S = np.zeros(X.shape[0])
    S[0] = X[0]
    for t in range(1, X.shape[0]):
        S[t] = alpha * X[t - 1] + (1 - alpha) * S[t - 1]
    return S


def double_exponential_smoothing(X, alpha, beta):
    S, A, B = (np.zeros(X.shape[0]) for i in range(3))
    S[0] = X[0]
    B[0] = X[1] - X[0]
    for t in range(1, X.shape[0]):
        A[t] = alpha * X[t] + (1 - alpha) * S[t - 1]
        B[t] = beta * (A[t] - A[t - 1]) + (1 - beta) * B[t - 1]
        S[t] = A[t] + B[t]
    return S


def triple_exponential_smoothing(X, L, alpha, beta, gamma, phi):
    def sig_phi(phi_p, m_p):
        return np.sum(np.array([np.power(phi_p, i) for i in range(m_p + 1)]))

    C, S, B, F = (np.zeros(X.shape[0]) for i in range(4))
    S[0], F[0] = X[0], X[0]
    B[0] = np.mean(X[L:2 * L] - X[:L]) / L
    m = 12
    sig_phi = sig_phi(phi, m)
    for t in range(1, X.shape[0]):
        S[t] = alpha * (X[t] - C[t % L]) + (1 - alpha) * (S[t - 1] + phi * B[t - 1])
        B[t] = beta * (S[t] - S[t - 1]) + (1 - beta) * phi * B[t - 1]
        C[t % L] = gamma * (X[t] - S[t]) + (1 - gamma) * C[t % L]
        F[t] = S[t] + sig_phi * B[t] + C[t % L]
    return S
